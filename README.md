## Build reqs

Fedora pkgs:

* ghostscript
* ImageMagick
* djvulibre, python2-djvulibre
* tesseract, tesseract-langpack-ukr

Not in Fedora repos:

* ocrodjvu

Other:

* `npm i -g djvused2pdfmark`
