.DELETE_ON_ERROR:
src := src
out := _out
mkdir = @mkdir -p $(dir $@)

book := $(out)/jan_slauerhoff--such_is_life_in_china
pages.src := $(wildcard $(src)/*tif)
pages.dest = $(patsubst $(src)/%.tif, $(out)/%$1, $(pages.src))

all: $(book).djvu $(book).pdf



$(book).djvu: meta.txt $(out)/cover.djvu $(call pages.dest,.djvu)
	djvm -c $@ $(sort $(filter %.djvu,$^))
	djvused -e 'set-meta meta.txt' $@ -s

$(out)/%.djvu: $(src)/%.jpg
	$(mkdir)
	c44 -slice 64,77,87 -dpi 600 $< $@

$(out)/%.djvu: $(src)/%.tif
	$(mkdir)
	cjb2 -lossy $< $@
	ocrodjvu -l ukr+eng --in-place $@ 2>/dev/null



$(book).pdf: meta.txt $(out)/cover.pdf $(call pages.dest,.pdf)
	djvused2pdfmark meta < meta.txt > $@.pdfmarks
	gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile=$@ $(sort $(filter %.pdf,$^)) $@.pdfmarks
	rm $@.pdfmarks

$(out)/%.pdf: $(src)/%.jpg
	$(mkdir)
	convert $< $@

$(out)/%.pdf: $(src)/%.tif
	$(mkdir)
	tesseract -l ukr+eng $< $(out)/$(basename $(notdir $<)) pdf 2>/dev/null
